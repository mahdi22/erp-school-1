package tn.esprit.erpschool.entites;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "t_module")
@NamedQueries({
		@NamedQuery(name = "findModuleByLabel", query = "select m from Module m where m.label=:label "),
		@NamedQuery(name = "findModuleByEctsNumber", query = "select m from Module m where m.ectsNumber=:ectsNumber"),
		@NamedQuery(name = "findAllModules", query = "select m from Module m ") })
public class Module implements Serializable {

	private ModulePK pk;
	private int hours;
	private String label;
	private int ectsNumber;
	private Teacher teacher;
	private Classe classe;

	public Module() {
	}

	public Module(ModulePK pk, int hours, String label, int ectsNumber) {
		super();
		this.pk = pk;
		this.hours = hours;
		this.label = label;
		this.ectsNumber = ectsNumber;
	}

	@EmbeddedId
	public ModulePK getPk() {
		return pk;
	}

	public void setPk(ModulePK pk) {
		this.pk = pk;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getEctsNumber() {
		return ectsNumber;
	}

	public void setEctsNumber(int ectsNumber) {
		this.ectsNumber = ectsNumber;
	}

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(referencedColumnName = "id", name = "idTeacher", insertable = false, updatable = false)
	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(referencedColumnName = "id", name = "idClasse", insertable = false, updatable = false)
	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	private static final long serialVersionUID = 1L;

}
