package tn.esprit.erpschool.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "t_student")
@NamedQueries({
		@NamedQuery(name = "findStudentByFirstNameAndLastName", query = "select s from Student s where s.firstName=:firstName and s.lastName=:lastName"),
		@NamedQuery(name = "findAllStudents", query = "select s from Student s ") })
public class Student extends Person implements Serializable {

	private boolean credit;
	private Classe classe;

	public Student(String firstName, String lastName, String email,
			String password, Date birthdate, boolean gender, boolean credit) {
		super(firstName, lastName, email, password, birthdate, gender);
		this.credit = credit;
	}

	public Student(String firstName, String lastName, String email,
			String password, Date birthdate, boolean gender, boolean credit,
			Classe classe) {
		super(firstName, lastName, email, password, birthdate, gender);
		this.credit = credit;
		this.classe = classe;
	}

	public Student() {
	}

	public boolean isCredit() {
		return credit;
	}

	public void setCredit(boolean credit) {
		this.credit = credit;
	}

	@ManyToOne
	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	private static final long serialVersionUID = 1L;

}
