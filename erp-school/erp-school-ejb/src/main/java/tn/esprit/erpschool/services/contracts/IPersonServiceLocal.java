package tn.esprit.erpschool.services.contracts;

import javax.ejb.Local;

import tn.esprit.erpschool.entites.Person;
import tn.esprit.erpschool.exceptions.BadCredentialsException;
import tn.esprit.erpschool.exceptions.MoreThanOneResultException;
import tn.esprit.erpschool.exceptions.NoResultFoundException;

@Local
public interface IPersonServiceLocal {

	public Person findPersonByCredentials(String email,
			String password) throws BadCredentialsException;
	
	public Person findPersonByfirstNameAndLastName(String firstName,
			String lastName)
			throws MoreThanOneResultException, NoResultFoundException;

	public Person findPersonByEmail(String email)
			throws MoreThanOneResultException, NoResultFoundException;
	
	public boolean isPersonEmailUnique(String email)
			throws MoreThanOneResultException, NoResultFoundException;
}
